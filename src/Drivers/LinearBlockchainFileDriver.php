<?php

namespace judahnator\BlockchainLogger\Drivers;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\BlockChain;
use function judahnator\BlockChain\createOriginBlock;
use judahnator\BlockChain\Drivers\BlockStorageInterface;
use judahnator\BlockChain\Exceptions\BlockNotFoundException;

class LinearBlockchainFileDriver implements BlockStorageInterface
{

    private $logFilePath;

    public function __construct(string $logFilePath)
    {
        $this->logFilePath = $logFilePath;
    }

    private static function parseLogEntry(string $entry)
    {
        $blockJson = json_decode($entry);
        if (json_last_error() !== JSON_ERROR_NONE || !$blockJson) {
            return null;
        }
        return new Block(
            $blockJson->height,
            $blockJson->previousHash,
            (new \DateTime())->setTimestamp($blockJson->created_at),
            $blockJson->data
        );
    }

    public function children(Block $block): array
    {
        $blocks = [];
        $handle = fopen("inputfile.txt", "r");
        while (($line = fgets($handle)) !== false) {
            $thisblock = self::parseLogEntry($line);
            if ($thisblock->hash === $block->hash) {
                $blocks[] = $thisblock;
            }
        }
        fclose($handle);
        return $blocks;
    }

    public function delete(Block $block): void
    {
        throw new \LogicException('You are trying to erase a log entry?');
    }

    public function find(string $blockHash): Block
    {
        $handle = fopen($this->logFilePath, "r");
        while (($line = fgets($handle)) !== false) {
            $block = self::parseLogEntry($line);
            if ($block->hash === $blockHash) {
                return $block;
            }
        }
        fclose($handle);
        throw new BlockNotFoundException('The given block could not be found');
    }

    public function getMostRecentEntry(): Block
    {
        return self::parseLogEntry(tailCustom($this->logFilePath)) ?: BlockChain::originBlock();
    }

    public function originBlock(\stdClass $defaultData = null): Block
    {
        return createOriginBlock($defaultData ?: (object)[
            'level'   => 'info',
            'message' => 'log opened',
            'context' => []
        ]);
    }

    public function save(Block $block): void
    {
        file_put_contents(
            $this->logFilePath,
            json_encode([
                'height'       => $block->height,
                'previousHash' => $block->previous->hash ?? '',
                'created_at'   => $block->created_at->getTimestamp(),
                'data'         => $block->data
            ]) . PHP_EOL,
            FILE_APPEND
        );
    }
}
