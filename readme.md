BlockChain PSR Logger
=====================

This is an implementation of the PSR logger that allows you to store
your system logs in a blockchain.

Do I need this?
---------------

No.

Why would anyone need this?
---------------------------

If you wanted to ensure your logs had not been tampered with, this might
not be a terrible option.

How do I use this?
------------------

If you are using something like Monolog, you can follow this template.
```php
<?php
use judahnator\BlockchainLogger\BlockchainLogger;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('my_logger');
$log->pushHandler(new BlockChainLogger('path/to/your.log'));
```

Also the suicide hotline can be reached at: 1-800-273-8255